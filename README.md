# Dobble game (and card deck generator)

## How to run?

If you don't know **`Dobble`** card game, google it at first. I will help you understand, what the task is.

- Make sure you have Node.js in verion 12 or higher.
- Run `npm install`.
- Run `npm start`. A new card in your browser should open after a few seconds.
- Choose number of symbols per card (8 preferably).
- Upload required number of various images (use images with transparent background to make cards look the most beautiful).
- Click `Stwórz talię` button.
- Click `Rozpocznij grę` button.
- Play Dobble against the computer (artifiszyl intelidżens).
