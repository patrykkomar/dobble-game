import 'bootstrap/dist/css/bootstrap.css';
import React, { Component } from 'react';
import { v4 as uuid } from 'uuid';

import '../styles/style.css';
import { generateDobbleDeck } from '../utils/dobble';
import Card from './card';
import FileUploader from './file-uploader';
// import Logo from './logo';
// import Navbar from './navbar';
import { shuffle } from '../utils/shuffle';

class App extends Component {
  state = {
    symbolsPerCard: 3,
    symbolsNeeded: 2 * 3 + 1,
    availableSymbolsPerCardNumbers: [3, 4, 5, 8],
    symbols: [],
    cards: [],
    gameCards: [],
    playerCard: null,
    computerCard: null,
    gameStarted: false,
    gameFinished: false,
    computerScore: 0,
    playerScore: 0,
    displayPlayerScoreSign: false,
    displayComputerScoreSign: false,
    failedAttempt: false,
  };

  render() {
    return (
      <React.Fragment>
        <div className='container-fluid'>
          {/* <div className='row'>
            <div className='col-lg-1'>
              <Logo onClick={this.handleLogoClick} />
            </div>
            <div className='col-lg-11'>
              <Navbar />
            </div>
          </div> */}
          <div>
            <h2>Liczba symboli na karcie</h2>
            <div>
              {this.state.availableSymbolsPerCardNumbers.map((n) => (
                <button
                  className={this.getNumberButtonClass(n)}
                  onClick={() => this.changeSymbolsNumber(n)}
                  key={`btn-${n}`}
                >
                  {n}
                </button>
              ))}
            </div>
            <div></div>
            {this.state.symbols.length} / {this.state.symbolsNeeded} symboli
            <button
              className={this.getGenerateButtonClass()}
              onClick={this.handleCardsGenerate}
            >
              Stwórz talię
            </button>
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <FileUploader
                onDelete={this.handleSymbolDelete}
                onChange={this.handleFileInputChange}
                symbols={this.state.symbols}
              />
            </div>
          </div>
          {/* <div>
            {this.state.cards.map((c, i) => (
              <Card
                symbols={c}
                key={`card${i}`}
                symbolsNumber={this.state.symbolsPerCard}
                playable={false}
              />
            ))}
          </div> */}
          <div>
            <button
              className={this.getGameButtonClass()}
              onClick={this.handleGameStarted}
            >
              Rozpocznij grę
            </button>
          </div>
          {this.state.gameStarted ? (
            <div>
              <div
                className='d-in-bl'
                style={{ height: '30px', width: '100%' }}
              >
                <img
                  src={
                    this.state.failedAttempt
                      ? 'https://www.vhv.rs/dpng/d/0-3669_red-cross-clipart-hd-png-download.png'
                      : 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Tick_green_modern.svg/1200px-Tick_green_modern.svg.png'
                  }
                  alt=''
                  style={
                    this.state.displayPlayerScoreSign
                      ? { height: '30px', marginRight: '50px' }
                      : {
                          height: '30px',
                          marginRight: '50px',
                          visibility: 'hidden',
                        }
                  }
                />
                {this.state.playerScore} : {this.state.computerScore}
                <img
                  src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Tick_green_modern.svg/1200px-Tick_green_modern.svg.png'
                  alt=''
                  style={
                    this.state.displayComputerScoreSign
                      ? { height: '30px', marginLeft: '50px' }
                      : {
                          height: '30px',
                          marginLeft: '50px',
                          visibility: 'hidden',
                        }
                  }
                />
              </div>
              <Card
                symbols={this.state.playerCard}
                key='player-card'
                symbolsNumber={this.state.symbolsPerCard}
                playable={false}
              />
              {this.state.gameFinished ? (
                ''
              ) : (
                <Card
                  symbols={this.state.gameCards[0]}
                  key='card-to-compare'
                  symbolsNumber={this.state.symbolsPerCard}
                  playable={true}
                  onClick={this.handlePlayerAttempt}
                />
              )}
              <Card
                symbols={this.state.computerCard}
                key='computer-card'
                symbolsNumber={this.state.symbolsPerCard}
                playable={false}
              />
            </div>
          ) : (
            ''
          )}
        </div>
      </React.Fragment>
    );
  }

  getGenerateButtonClass = () => {
    return this.state.symbols.length >= this.state.symbolsNeeded
      ? 'active'
      : 'inactive';
  };

  getNumberButtonClass = (n) => {
    return this.state.symbolsPerCard === n ? 'active' : 'inactive';
  };

  getGameButtonClass = (n) => {
    return this.state.cards.length > 0 ? 'active' : 'inactive';
  };

  changeSymbolsNumber = (n) => {
    if (this.state.symbolsPerCard !== n) {
      this.setState({
        symbolsPerCard: n,
        symbolsNeeded: (n - 1) * n + 1,
        cards: [],
      });
    }
  };

  handleLogoClick = () => {};

  handleFileInputChange = (event) => {
    const symbols = this.state.symbols.map((s) => ({ ...s }));
    const files = event.target.files;
    if (files) {
      for (const file of files) {
        const id = uuid();
        symbols.push({
          id,
          image: URL.createObjectURL(file),
        });
      }

      this.setState({ symbols: symbols });
    }
  };

  handleSymbolDelete = (symbolId) => {
    const index = this.state.symbols.findIndex((s) => s.id === symbolId);

    if (index === -1) {
      return;
    }

    const symbols = this.state.symbols.map((s) => ({ ...s }));

    symbols.splice(index, 1);

    this.setState({ symbols });
  };

  handleCardsGenerate = () => {
    if (this.state.symbols.length < this.state.symbolsNeeded) {
      return;
    }

    const { symbols, symbolsPerCard } = this.state;
    const cards = generateDobbleDeck(symbolsPerCard, symbols).map((card) =>
      card.map((s) => ({ ...s, rotation: Math.floor(Math.random() * 360) }))
    );

    this.setState({ cards });
  };

  handlePlayerAttempt = (symbolId) => {
    const matchingElement = this.state.playerCard.find(
      ({ id }) => id === symbolId
    );

    if (matchingElement) {
      const playerScore = this.state.playerScore + 1;
      this.setState({ playerScore });
    } else {
      const computerScore = this.state.computerScore + 1;
      this.setState({ computerScore });
    }

    const gameCards = this.state.gameCards.map((card) =>
      card.map((s) => ({ ...s }))
    );

    const [card] = gameCards.splice(0, 1);

    const stateModification = matchingElement
      ? {
          gameCards,
          playerCard: card,
          displayPlayerScoreSign: true,
          failedAttempt: false,
        }
      : {
          gameCards,
          computerCard: card,
          displayPlayerScoreSign: true,
          failedAttempt: true,
        };

    if (gameCards.length === 0) {
      stateModification.gameFinished = true;
    }

    this.setState(stateModification);

    setTimeout(() => {
      this.setState({ displayPlayerScoreSign: false });
    }, 1000);

    setTimeout(() => {
      this.makeComputerThink();
    }, 100);
  };

  handleGameStarted = () => {
    if (!this.state.cards.length) {
      return;
    }

    const gameCards = this.state.cards.map((card) =>
      card.map((s) => ({ ...s }))
    );

    const [playerCard] = gameCards.splice(
      Math.floor(Math.random() * gameCards.length),
      1
    );
    const [computerCard] = gameCards.splice(
      Math.floor(Math.random() * gameCards.length),
      1
    );

    this.setState(
      {
        computerCard,
        gameCards,
        playerCard,
        gameStarted: true,
        gameFinished: false,
        playerScore: 0,
        computerScore: 0,
      },
      this.makeComputerThink
    );
  };

  async makeComputerThink() {
    const computerCard = shuffle(
      this.state.computerCard.map((s) => ({ ...s }))
    );
    const initialGameCardsNumber = this.state.gameCards.length;
    for (const symbol of computerCard) {
      await new Promise((res) => {
        setTimeout(res, 1000);
      });

      if (this.state.gameCards.length < initialGameCardsNumber) {
        break;
      }

      if (
        this.state.gameCards[0] &&
        this.state.gameCards[0].find((s) => s.id === symbol.id)
      ) {
        const gameCards = this.state.gameCards.map((card) =>
          card.map((s) => ({ ...s }))
        );
        const [card] = gameCards.splice(0, 1);

        const stateModification = {
          gameCards,
          computerCard: card,
          displayComputerScoreSign: true,
          computerScore: this.state.computerScore + 1,
        };

        if (gameCards.length === 0) {
          stateModification.gameFinished = true;
        }

        this.setState(stateModification);

        setTimeout(() => {
          this.setState({ displayComputerScoreSign: false });
        }, 1000);

        setTimeout(() => {
          this.makeComputerThink();
        }, 100);

        break;
      }
    }
  }
}

export default App;
