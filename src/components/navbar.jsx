import React, { Component } from 'react';

class Navbar extends Component {
  state = {};

  styles = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  };

  render() {
    return (
      <nav style={this.styles}>
        <span>Generowanie talii</span>
        <span>Grank</span>
      </nav>
    );
  }
}

export default Navbar;
