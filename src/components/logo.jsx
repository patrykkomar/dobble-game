import React, { Component } from 'react';

class Logo extends Component {
  state = {
    id: 'logo',
    src: 'https://lmiuk.com/wp-content/uploads/2019/12/Dobble_Pose_11.png',
    title: 'Dobble',
  };

  render() {
    const { id, src, title } = this.state;
    return (
      <img
        id={id}
        src={src}
        alt={title}
        title={title}
        onClick={this.props.onClick}
      />
    );
  }
}

export default Logo;
