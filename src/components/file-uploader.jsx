import React, { Component } from 'react';

class FileUploader extends Component {
  state = {};

  render() {
    return (
      <React.Fragment>
        <div>
          <input type='file' multiple onChange={this.props.onChange} />
        </div>
        <div>
          {this.props.symbols.map(({ id, image }) => (
            <div className='d-in-bl symbol' key={'div-' + id}>
              <img
                className='symbol'
                id={id}
                src={image}
                alt={id}
                key={'image-' + id}
              />
              <button
                className='symbol-delete'
                onClick={() => this.props.onDelete(id)}
              >
                X
              </button>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default FileUploader;
