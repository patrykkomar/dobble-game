import React, { Component } from 'react';

import { symbolsPositionsAndSizes } from '../utils/dobble';

class Card extends Component {
  state = {};

  render() {
    const { symbols, symbolsNumber } = this.props;

    return (
      <React.Fragment>
        <div className={this.getCardClass()}>
          {symbols.map(({ id, image, rotation }, i) => (
            <img
              key={id}
              src={image}
              alt={id}
              title={id}
              style={{
                maxWidth: symbolsPositionsAndSizes[symbolsNumber][i].size,
                left: symbolsPositionsAndSizes[symbolsNumber][i].position.left,
                top: symbolsPositionsAndSizes[symbolsNumber][i].position.top,
                transform: `rotate(${rotation}deg`,
              }}
              onClick={() => this.handleSymbolClick(id)}
            />
          ))}
        </div>
      </React.Fragment>
    );
  }

  handleSymbolClick = (id) => {
    const { onClick, playable } = this.props;

    if (!playable) {
      return;
    }

    onClick(id);
  };

  getCardClass = () => {
    return this.props.playable ? 'd-in-bl card playable' : 'd-in-bl card';
  };
}

export default Card;
