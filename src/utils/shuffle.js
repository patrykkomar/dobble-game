export function shuffle(array = []) {
  const result = [];
  while (array.length) {
    const random = Math.floor(Math.random() * array.length);
    result.push(array[random]);
    array.splice(random, 1);
  }
  return result;
}
