import { shuffle } from './shuffle';

export function generateDobbleDeck(numberOfSymbolsOnCard, symbols) {
  const symbolsCopy = symbols.map(s => ({ ...s }));
  const shuffledSymbols = shuffle(symbolsCopy);
  let cards = [];
  const n = numberOfSymbolsOnCard - 1;
  for (let i = 0; i <= n; i++) {
    cards.push([1]);
    for (let j = 0; j < n; j++) {
      cards[i].push((j + 1) + (i * n) + 1)
    }
  }
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      cards.push([i + 2]);
      for (let k = 0; k < n; k++) {
        const val = (n + 1 + n * k + (i * k + j) % n) + 1;
        cards[cards.length - 1].push(val);
      }
    }
  }
  cards = cards.map(card => card.map(s => shuffledSymbols[s - 1]));
  return shuffle(cards).map(shuffle);
}

export const symbolsPositionsAndSizes = [
  null,
  null,
  null,
  [
    { size: '40%', position: { top: '12.5%', left: '12.5%' } },
    { size: '35%', position: { top: '57.5%', left: '35%' } },
    { size: '25%', position: { top: '25%', left: '65%' } },
  ],
  [
    { size: '40%', position: { top: '35%', left: '50%' } },
    { size: '30%', position: { top: '15%', left: '12.5%' } },
    { size: '25%', position: { top: '60%', left: '20%' } },
    { size: '20%', position: { top: '10%', left: '50%' } },
  ],
  [
    { size: '35%', position: { top: '30%', left: '55%' } },
    { size: '30%', position: { top: '15%', left: '12.5%' } },
    { size: '25%', position: { top: '57.5%', left: '22.%' } },
    { size: '20%', position: { top: '5%', left: '45%' } },
    { size: '20%', position: { top: '70%', left: '55%' } },
  ],
  [],
  null,
  [
    // { size: '32.5%', position: { top: '27.5%', left: '31%' } },
    // { size: '25%', position: { top: '42.5%', left: '67.5%' } },
    // { size: '22.5%', position: { top: '65%', left: '15%' } },
    // { size: '20%', position: { top: '15%', left: '65%' } },
    // { size: '17.5%', position: { top: '2.5%', left: '35%' } },
    // { size: '17.5%', position: { top: '70%', left: '42.5%' } },
    // { size: '17.5%', position: { top: '32.5%', left: '7.5%' } },
    // { size: '15%', position: { top: '72.5%', left: '67.5%' } },
    { size: '30%', position: { top: '30%', left: '32%' } },
    { size: '27.5%', position: { top: '42.5%', left: '67.5%' } },
    { size: '25%', position: { top: '55%', left: '11%' } },
    { size: '22.5%', position: { top: '15%', left: '65%' } },
    { size: '20%', position: { top: '2.5%', left: '35%' } },
    { size: '17.5%', position: { top: '72.5%', left: '40%' } },
    { size: '15%', position: { top: '32.5%', left: '7.5%' } },
    { size: '12.5%', position: { top: '72.5%', left: '65%' } },
  ],
];
